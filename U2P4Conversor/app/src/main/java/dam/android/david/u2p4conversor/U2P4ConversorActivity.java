package dam.android.david.u2p4conversor;


import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;


public class U2P4ConversorActivity extends LogActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI() {
        EditText etUnidad = findViewById(R.id.et_Unidad);
        EditText etResultado = findViewById(R.id.et_Resultado);

        //TODO 2. Mostrar error que se ha producido también por pantalla

        TextView tvError = findViewById(R.id.tv_Error);
        tvError.setVisibility(View.INVISIBLE);

        Button buttonConvertir = findViewById(R.id.button_Convertir);

        //TODO 1. Conversión en ambos sentidos

        ToggleButton toggle = findViewById(R.id.toggleButton); /** Creamos un Toggle button para alternar de una covnersión a otra*/

        buttonConvertir.setOnClickListener(view -> {
            try {
                tvError.setVisibility(View.INVISIBLE);
                etResultado.setText(convertInchCm(etUnidad.getText().toString()));
                Log.i(LogActivity.DEBUG_TAG, "Botón Convertir pulsado");

            } catch (Exception e ) {
                Log.e(LogActivity.DEBUG_TAG, "Introduce al menos 1 pulgada");
                tvError.setVisibility(View.VISIBLE);
            }
        });

        toggle.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                Log.i(LogActivity.DEBUG_TAG, "Botón Toggle en modo: Centimetros");
                buttonConvertir.setOnClickListener(view -> {
                    try {
                        tvError.setVisibility(View.INVISIBLE);
                        etResultado.setText(ConvertCmInch(etUnidad.getText().toString()));
                        Log.i(LogActivity.DEBUG_TAG, "Botón Convertir pulsado");

                    } catch (Exception e) {
                        Log.e(LogActivity.DEBUG_TAG, "Introduce al menos 1 centimetro");
                        tvError.setVisibility(View.VISIBLE);
                    }
                });
            } else Log.i(LogActivity.DEBUG_TAG, "Botón Toggle en moddo: Pulgadas");

        });
    }

    private String convertInchCm(String pulgadaText) throws Exception {

        double pulgadaValue = Double.parseDouble(pulgadaText);
        double resultado = pulgadaValue * 2.54;

        //TODO 2. Restringe el método convertir para que no acepte numeros < 1
        if (pulgadaValue < 1) throw new Exception();

        //TODO 1. Formatea el resultado mostrado con dos decimales
        return String.format("%.2f", resultado);
    }

    private String ConvertCmInch(String centimetroText) throws Exception { /** Creamos un método inverso para convertir de cm a pulgadas, para que sea llamado al activar el toggle */
        double centimetroValue = Double.parseDouble(centimetroText);
        double resultado = centimetroValue / 2.54;

        if (centimetroValue < 1) throw new Exception();
        return String.format("%.2f", resultado);
    }
}